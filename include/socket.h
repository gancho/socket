/*
 * Copyright © 2023 Todor Ganchovsky
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _SOCKET_H_
#define _SOCKET_H_

#include <chrono>
#include <string>
#include <vector>
#include <netdb.h>
#include <netinet/in.h>

using byte = unsigned char;

struct basic_socket
{
	basic_socket() : _socket(-1), _host(std::string{}), _port(0), _again(false)
	{
	}
	~basic_socket()
	{
	}
	basic_socket(const basic_socket&) = delete;			   // no copy constructor
	basic_socket& operator=(const basic_socket&) = delete; // no assignment

	static int network_error(const int error, const std::string& message);
	static int host_entry(const std::string& host, hostent& entry);
	static int select(fd_set* read, fd_set* write, fd_set* except, const timeval& timeout);

	int open(const int domain, const int type, const int protocol);
	virtual int close();
	enum class disable
	{				 // used with shutdown
		receive = 0, // SHUT_RD
		send,		 // SHUT_WR
		both		 // SHUT_RDWR
	};
	int shutdown(disable how);
	int connect(const sockaddr_in& address);
	int connect(const std::string& host, const uint16_t port);

	int send(const byte* data, const size_t length, const uint32_t flags);
	int receive(byte* data, const size_t length, const uint32_t flags);
	int receive(byte* data, const size_t length, const uint32_t flags, const timeval& timeout);

	int get_option(const int level, const int option, byte* value, int& length);
	int set_option(const int level, const int option, const byte* value, const int length);

  protected:
	uint32_t _socket;
	std::string _host;
	uint16_t _port;
	bool _again;
	std::chrono::steady_clock::time_point _connected_since;
	std::chrono::steady_clock::time_point _last_activity;
	std::chrono::milliseconds _elapsed_since_last_activity;

  public:
	std::string host() const;
	uint16_t port() const;
	uint64_t uptime() const;
	uint64_t elapsed() const;
};

/*
 * TCP/IP socket
 * A socket type that provides sequenced, reliable, two-way, connection-based byte streams with an OOB data transmission mechanism. This socket type uses the
 * Transmission Control Protocol (TCP) for the Internet address family (AF_INET or AF_INET6)
 */
struct stream_socket : public basic_socket
{
	stream_socket() : basic_socket(), _aborted(false)
	{
	}
	~stream_socket()
	{
	}
	stream_socket(const stream_socket&) = delete;			 // no copy constructor
	stream_socket& operator=(const stream_socket&) = delete; // no assignment

	int open(const std::string& host, const uint16_t port);
	virtual int close();

	size_t send(const std::vector<byte>& data);
	std::vector<byte> receive();

	// enables or disables the Nagle algorithm for TCP sockets
	int set_tcp_no_delay(const bool enable = false);
	int set_non_blocking_mode();

	bool aborted() const;

  private:
	bool _aborted;
};

#endif // !_SOCKET_H_

