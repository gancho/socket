/*
 * Copyright © 2023 Todor Ganchovsky
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "socket.h"
#include <cassert>
#include <cstring>
#include <sstream>
#include <iostream>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <unistd.h>

int basic_socket::network_error(const int error, const std::string& message)
{
	std::string description;
#if defined(_WIN32) || defined(_WIN64)
	description.resize(256);
	FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), &description[0], description.length(), NULL);
#else
	description = std::strerror(error);
#endif
	if (description.empty())
	{
		description = "Unkown network error: " + std::to_string(error);
	}
	std::cout << description << " - " << message;
	return error;
}

int basic_socket::host_entry(const std::string& host, hostent& entry)
{
	auto tmp = ::gethostbyname(host.c_str());
	if (tmp == nullptr)
	{
		// maybe an IP address
		if (std::atoi(host.c_str()) == 0)
		{
			std::ostringstream oss;
			oss << "The host " << host << " was not found";
			return network_error(EINVAL, oss.str());
		}
		// indeed an IP address
		auto address = ::inet_addr(host.c_str());
		if (address == INADDR_NONE)
		{
			std::ostringstream oss;
			oss << host << " isn't a legitimate Internet address";
			return network_error(EINVAL, oss.str());
		}
		tmp = ::gethostbyaddr(host.c_str(), 4, AF_INET);
		if (tmp == nullptr)
		{
			std::ostringstream oss;
			oss << "The host with IP address " << host << " was not found";
			return basic_socket::network_error(errno, oss.str());
		}
	}
	memcpy(&entry, tmp, sizeof(hostent));

	return 0;
}

int basic_socket::select(fd_set* read, fd_set* write, fd_set* except, const timeval& timeout)
{
	int max = 0;
	if (read != nullptr)
	{
		for (auto i = 0; i < FD_SETSIZE; i++)
		{
			max = std::max(FD_ISSET(i, read) ? i : 0, max);
		}
	}
	if (write != nullptr)
	{
		for (auto i = 0; i < FD_SETSIZE; i++)
		{
			max = std::max(FD_ISSET(i, write) ? i : 0, max);
		}
	}
	if (except != nullptr)
	{
		for (auto i = 0; i < FD_SETSIZE; i++)
		{
			max = std::max(FD_ISSET(i, except) ? i : 0, max);
		}
	}
	return ::select(max + 1, read, write, except, const_cast<timeval*>(&timeout));
}

std::string basic_socket::host() const
{
	return _host;
}

uint16_t basic_socket::port() const
{
	return _port;
}

int basic_socket::open(const int domain, const int type, const int protocol)
{
	if (_socket != -1)
	{
		basic_socket::close();
	}
	assert(_socket == -1);
	_socket = ::socket(domain, type, protocol);
	if (_socket < 0)
	{
		_socket = -1;
		return basic_socket::network_error(errno, "(socket)");
	}
	return 0;
}

int basic_socket::close()
{
	if (_socket == -1)
	{
		// already closed
		return 0;
	}
	int st = 0;
#if defined(_WIN32) || defined(_WIN64)
	st = ::closesocket(_socket);
	if (st < 0)
	{
		st = WSAGetLastError();
	}
#else
	do
	{
		st = ::close(_socket);
	} while ((st < 0) && (errno == EINTR));
#endif
	_socket = -1;
	_host.clear();
	_port = 0;

	return st;
}

int basic_socket::shutdown(disable how)
{
	if (_socket == -1)
	{
		return basic_socket::network_error(ENOTSOCK, "shutdown");
	}
	return ::shutdown(_socket, static_cast<int>(how));
}

int basic_socket::connect(const sockaddr_in& address)
{
	if (_socket == -1)
	{
		return basic_socket::network_error(ENOTSOCK, "connect");
	}
	auto st = ::connect(_socket, reinterpret_cast<const sockaddr*>(&address), sizeof(address));
	_connected_since = std::chrono::steady_clock::now();
	_last_activity = std::chrono::steady_clock::now();
	_elapsed_since_last_activity = std::chrono::duration_cast<std::chrono::milliseconds>(_last_activity - _connected_since);
	return st;
}

int basic_socket::connect(const std::string& host, const uint16_t port)
{
	hostent entry;
	auto st = basic_socket::host_entry(host, entry);
	if (st < 0)
	{
		std::cout << "socket connect (" << host << ":" << port << ") failed with error " << st;
		return st;
	}
	sockaddr_in address;
	memset(&address, 0, sizeof(address));
	memcpy(&(address.sin_addr), entry.h_addr, entry.h_length);
	address.sin_family = entry.h_addrtype; // AF_INET
	address.sin_port = htons(port);
	st = basic_socket::connect(address);
	if (st >= 0)
	{
		_host = host;
		_port = port;
	}
	return st;
}

int basic_socket::send(const byte* data, const size_t length, const uint32_t flags)
{
	if (_socket == -1)
	{
		return basic_socket::network_error(ENOTSOCK, "send");
	}
	auto st = ::send(_socket, reinterpret_cast<const char*>(data), static_cast<const int>(length), flags);
	_elapsed_since_last_activity = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _last_activity);
	_last_activity = std::chrono::steady_clock::now();
	return st;
}

int basic_socket::receive(byte* data, const size_t length, const uint32_t flags)
{
	if (_socket == -1)
	{
		return basic_socket::network_error(ENOTSOCK, "receive");
	}
	auto st = ::recv(_socket, reinterpret_cast<char*>(data), static_cast<int>(length), flags);
	_elapsed_since_last_activity = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _last_activity);
	_last_activity = std::chrono::steady_clock::now();
	return st;
}

int basic_socket::receive(byte* data, const size_t length, const uint32_t flags, const timeval& timeout)
{
	_again = false;
#if defined(_WIN32) || defined(_WIN64)
	DWORD tv = 0;
#else
	timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 0;
#endif
	int len = sizeof(tv);
	auto st = basic_socket::get_option(SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<byte*>(&tv), len);
	if (st < 0)
	{
		return st;
	}
	// set timeout
#if defined(_WIN32) || defined(_WIN64)
	DWORD msec = timeout.tv_sec * 1000 + timeout.tv_usec / 1000;
	st = basic_socket::set_option(SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const byte*>(&msec), sizeof(msec));
#else
	st = basic_socket::set_option(SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const byte*>(&timeout), sizeof(timeout));
#endif
	if (st < 0)
	{
		return st;
	}
	auto bytes = basic_socket::receive(data, length, flags);
	if ((bytes < 0) && ((errno == EAGAIN) || (errno == EWOULDBLOCK)))
	{
		bytes = 0;
		_again = true;
	}
	// restore defaults
	st = basic_socket::set_option(SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const byte*>(&tv), sizeof(tv));
	return bytes;
}

int basic_socket::get_option(const int level, const int option, byte* value, int& length)
{
	if (_socket == -1)
	{
		return basic_socket::network_error(ENOTSOCK, "get_option");
	}
#if defined(_WIN32) || defined(_WIN64)
	return ::getsockopt(_socket, level, option, reinterpret_cast<char*>(value), &length);
#else
	return ::getsockopt(_socket, level, option, reinterpret_cast<char*>(value), reinterpret_cast<unsigned*>(&length));
#endif
}

int basic_socket::set_option(const int level, const int option, const byte* value, const int length)
{
	if (_socket == -1)
	{
		return basic_socket::network_error(ENOTSOCK, "set_option");
	}
	return ::setsockopt(_socket, level, option, reinterpret_cast<const char*>(value), static_cast<unsigned>(length));
}

uint64_t basic_socket::uptime() const
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(_last_activity - _connected_since).count();
}

uint64_t basic_socket::elapsed() const
{
	return _elapsed_since_last_activity.count();
}

int stream_socket::open(const std::string& host, const uint16_t port)
{
	auto st = basic_socket::open(AF_INET, SOCK_STREAM, 0);
	if (st < 0)
	{
		return st;
	}
	st = basic_socket::connect(host, port);
	if (st < 0)
	{
		return st;
	}
	// disable Nagle's algorithm
	return stream_socket::set_tcp_no_delay();
}

int stream_socket::close()
{
	auto st = basic_socket::shutdown(disable::both);
	if (st < 0)
	{
		return st;
	}
	return basic_socket::close();
}

size_t stream_socket::send(const std::vector<byte>& data)
{
	auto buffer = data.data();
	auto length = data.size();
	size_t result = 0;
	do
	{
#if defined(_WIN32) || defined(_WIN64)
		auto bytes = basic_socket::send(buffer, length, 0);
#else
		auto bytes = basic_socket::send(buffer, length, MSG_NOSIGNAL);
#endif
		if (bytes < 0)
		{
			// an error occured
#if defined(_WIN32) || defined(_WIN64)
			_aborted = (WSAGetLastError() == WSAECONNABORTED);
#else
			_aborted = (errno == EPIPE) || (errno == ENOTCONN);
#endif
			// aborted state indicates that the connection has not been kept open
			// => close the socket, open a new one and connect again
			break;
		}
		if (bytes >= 0)
		{
			result += bytes;
			length -= bytes;
			buffer += bytes;
		}
	} while (length > 0);
	return result;
}

std::vector<byte> stream_socket::receive()
{
	std::vector<byte> data;
	timeval timeout;
	timeout.tv_sec = 30; // 30 seconds
	timeout.tv_usec = 0;
	std::vector<byte> buffer;
	do
	{
		buffer.resize(8192); // a maximum header size of 8 kB by convention
		auto bytes = basic_socket::receive(&buffer[0], buffer.size(), 0, timeout);
		if (bytes < 0)
		{
			// an error occured
#if defined(_WIN32) || defined(_WIN64)
			_aborted = (WSAGetLastError() == WSAECONNABORTED);
#else
			_aborted = (errno == EPIPE) || (errno == ENOTCONN);
#endif
			data.clear();
			break;
		}
		if (bytes >= 0)
		{
			buffer.resize(bytes);
			data.insert(data.end(), buffer.begin(), buffer.end());
		}
	} while (_again);
	return data;
}

int stream_socket::set_tcp_no_delay(const bool enable)
{
	const int value = enable ? 0 : 1;
	auto st = basic_socket::set_option(IPPROTO_TCP, TCP_NODELAY, (const byte*)&value, sizeof(value));
	if (st < 0)
	{
		std::ostringstream oss;
		oss << _host << ':' << std::to_string(_port) << " (setsockopt)";
		return basic_socket::network_error(errno, oss.str());
	}
	return st;
}

int stream_socket::set_non_blocking_mode()
{
	int st = 0;
#if defined(LINUX) || defined(__linux__)
	auto flags = ::fcntl(_socket, F_GETFL, 0);
	if (-1 == flags)
	{
		std::ostringstream oss;
		oss << _host << ':' << std::to_string(_port) << " (fcntl)";
		return basic_socket::network_error(errno, oss.str());
	}
	st = ::fcntl(_socket, F_SETFL, flags | O_NONBLOCK);
	if (st < 0)
	{
		std::ostringstream oss;
		oss << _host << ':' << std::to_string(_port) << " (fcntl)";
		return basic_socket::network_error(errno, oss.str());
	}
#else
	const int one = 1;
	st = ::ioctlsocket(_socket, FIONBIO, (u_long*)&one);
	if (st < 0)
	{
		std::ostringstream oss;
		oss << _host << ':' << std::to_string(_port) << " (ioctlsocket)";
		return basic_socket::network_error(errno, oss.str());
	}
#endif
	return st;
}

bool stream_socket::aborted() const
{
	return _aborted;
}

